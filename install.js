const recorder = require('node-record-lpcm16')
const fs = require('fs')
const os = require('os')

var wakeWordsDir = os.homedir() + '/vicky/wakeWords/'
var wakeWordName = process.argv[2]
var dir = wakeWordsDir+wakeWordName
if(!fs.existsSync(dir)){
    fs.mkdir(dir, { recursive: true }, (err) => {
        if (err) throw err;
    });
}

while(!fs.existsSync(dir)){
  console.log("creation du dossier en cours")
}

var fileName = dir + '/' + wakeWordName + fs.readdirSync(dir).length + '.wav'
const file = fs.createWriteStream(fileName, { encoding: 'binary' })
 

recorder.record({
  sampleRate: 16000,
  recorder: 'sox',
  endOnSilence: true
})
.stream()
.pipe(file)